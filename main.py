#!/usr/bin/env python3
import argparse

# dd if=$1 of=mac_key bs=1 skip=6291456 count=4194304 &&
# cat begin mac_key end > $2


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", required=True)
    parser.add_argument("-o", required=True)
    args = parser.parse_args()

    with open(args.i, "rb") as source, open(args.o, "wb") as no_crs:
        while data := source.read(1024):
            no_crs.write(data)
            source.seek(32, 1)


if __name__ == "__main__":
    main()

# Repair 5151k
Программа для восстановления прошивки аппарата 5151k
На текущий момент только очищает дамп от контрольных сумм.

``` sh
./main.py -i dump -o no_crs
dd if=no_crs of=keys bs=1 skip=6291456 count=4194304
cat begin keys end > output
```
